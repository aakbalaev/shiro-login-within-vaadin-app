package kg.vaadin.example.jpa;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;

import kg.vaadin.example.domain.Permission;
import kg.vaadin.example.domain.Person;
import kg.vaadin.example.domain.Role;

public class JpaTest {

	private EntityManager em;
	private static IniSecurityManagerFactory iniFactory;
	private static SecurityManager securityManager;
	private static PasswordService passwordService;
	
	public JpaTest(EntityManager em) {
		this.em = em;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//iniFactory = new IniSecurityManagerFactory();
		//securityManager = iniFactory.getInstance();
		//SecurityUtils.setSecurityManager(securityManager);
		
		passwordService = new DefaultPasswordService();
		PasswordMatcher matcher = new PasswordMatcher();
		matcher.setPasswordService(passwordService);
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("persistenceUnit");
		EntityManager em = factory.createEntityManager();
		JpaTest test = new JpaTest(em);

		EntityTransaction tx = em.getTransaction();
		tx.begin();
		try {
			test.createEmployees();
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();

		System.out.println(".. done");
	}

	private void createEmployees() {
		
		long sizeU = (Long) em.createQuery("SELECT COUNT(p) FROM Person p").getSingleResult();
		if (sizeU == 0) {
			//Creating two users with passwords
			Person admin = new Person("admin", "admin@example.com", passwordService.encryptPassword("password"));
			Person manager = new Person("manager", "manager@example.com", passwordService.encryptPassword("manager"));
			Person guest = new Person("guest", "guest@example.com", passwordService.encryptPassword("guest"));
			
			
			// Creating three roles to ROLES table
			Role adminRole = new Role("*", "For an administrator only");
			Role managerRole = new Role("manager", "For managers only");
			Role staffRole = new Role("staff", "For staff only");
			Role simpleRole = new Role("simple", "For everybody");
			
			
			// Creating three permissions
			Permission read = new Permission("read", "Read permission");
			Permission write = new Permission("write", "Write permission");
			Permission execute = new Permission("execute", "Execute Permission");
			
			
			//========Adding roles to users========
			
			// Add Administrator Roles
			Set<Role> adminUserRoles = new HashSet<Role>();
			adminUserRoles.add(adminRole);
			admin.setRoles(adminUserRoles);
			
			// Add Administrator Permissions
			Set<Permission> adminPermissions = new HashSet<Permission>();
			adminPermissions.add(read);
			adminPermissions.add(write);
			adminPermissions.add(execute);
			// Add Administrator's permissions to Administrator's Role
			adminRole.setPermissions(adminPermissions);
			
			// Add Manager Roles
			Set<Role> managerUserRoles = new HashSet<Role>();
			managerUserRoles.add(managerRole);
			managerUserRoles.add(staffRole);
			manager.setRoles(managerUserRoles);
			// Add Manager Permissions
			Set<Permission> managerPermissions = new HashSet<Permission>();
			managerPermissions.add(read);
			managerPermissions.add(write);
			// Add Manager's permissions to Manager's Role(managerRole)
			managerRole.setPermissions(managerPermissions);
		
			//Saving users to USERS table
			em.persist(admin);
			em.persist(manager);
			em.persist(guest);
			
			//Saving roles to ROLES table
			em.persist(adminRole);
			em.persist(managerRole);
			em.persist(staffRole);
			em.persist(simpleRole); // simple Role by default
			
			//Saving permissions to PERMISSIONS table
			em.persist(execute);
			em.persist(write);
			em.persist(read);			
		}
	}
}
