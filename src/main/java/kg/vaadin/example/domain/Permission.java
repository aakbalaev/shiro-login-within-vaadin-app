package kg.vaadin.example.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="PERMISSIONS")
public class Permission {

	public Permission(){}
	public Permission(String name){
		this.name = name;
	}
	public Permission(String name, String description){
		this.name = name;
		this.description = description;
	}
	@Id
	@NotNull
	@Size(max = 30)
	private String name;

	@Size(max =255)
	private String description;
	
	@ManyToMany(mappedBy="permissions")
	private Set<Role> roles;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
}