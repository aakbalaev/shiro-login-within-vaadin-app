package kg.vaadin.example.domain;

import kg.vaadin.example.domain.Role;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name="USERS")
public class Person {
	
	public Person(){
	}
	public Person(String username, String password){
		this.username = username;
		this.password = password;
	}
	public Person(String username, String email, String password){
		this.username = username;
		this.email = email;
		this.password = password;
	}

	@Id
	@NotNull
	@Size(min=3, max=15)
	private String username;
	
	@Size(min=6, max=100)
	private String email;
	
	@NotNull
	@Size(max=255)
	private String password;
	
	@ManyToMany
	@JoinTable(name="USERS_ROLES", 
						joinColumns=@JoinColumn(name="username"), 
						inverseJoinColumns=@JoinColumn(name="role_name")
	)
	private Set<Role> roles;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
}
