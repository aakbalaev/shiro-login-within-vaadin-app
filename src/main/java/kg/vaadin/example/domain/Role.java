package kg.vaadin.example.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="ROLES")
public class Role {

	public Role(){}
	
	public Role(String name){
		this.name = name;
	}
	public Role(String name, String description){
		this.name = name;
		this.description = description;
	}
	@Id
	@NotNull
	@Size(max=20)
	private String name;

	@Size(max=255)
	private String description;
	
	@ManyToMany(mappedBy="roles")
	private Set<Person> persons;
	
	@ManyToMany
	@JoinTable(name="ROLES_PERMISSIONS", 
						joinColumns=@JoinColumn(name="role_name"), 
						inverseJoinColumns=@JoinColumn(name="permission")
	)
	private Set<Permission> permissions;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Person> getPersons() {
		return persons;
	}
	
	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	public void setPersons(Set<Person> persons) {
		this.persons = persons;
	}
}
