package kg.vaadin.example.shiro;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class LoginScreen extends VerticalLayout implements ClickListener, View { 

	//private final static Enum<VIEWS<String>>() =  {"secure", "register", "error"};
	private FormLayout formLayout = new FormLayout();
	private HorizontalLayout horizontalLayout = new HorizontalLayout();
	private Panel loginPanel = new Panel("Login");
	private TextField username = new TextField("Username");
	private PasswordField password = new PasswordField("Password");
	private Button loginBtn = new Button("Login", this);
	private Button registerBtn = new Button("Sign Up", this);
	
	public LoginScreen() {
		setSizeFull();
		loginPanel.setSizeUndefined();
		horizontalLayout.addComponent(registerBtn);
		horizontalLayout.addComponent(loginBtn);
		username.focus();
		formLayout.addComponent(username);
		formLayout.addComponent(password);
		formLayout.addComponent(horizontalLayout);
		formLayout.setSizeUndefined();
		formLayout.setMargin(true);
		loginPanel.setContent(formLayout);
		addComponent(loginPanel);
		setComponentAlignment(loginPanel, Alignment.MIDDLE_CENTER);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if(event.getSource() == loginBtn){
			Subject currentUser = SecurityUtils.getSubject();
			UsernamePasswordToken token = new UsernamePasswordToken(
					username.getValue(), password.getValue());
			try {
				currentUser.login(token);
				getUI().getNavigator().addView("/secure", SecureView.class);
				getUI().getNavigator().navigateTo("/secure");
				VaadinService.reinitializeSession(VaadinService.getCurrentRequest());
			} catch (Exception e) {
				Logger.getAnonymousLogger().log(Level.INFO, e.getMessage());
				username.setValue("");
				password.setValue("");
				Notification.show("Invalid username or password", Type.ERROR_MESSAGE);
			}
		}
		if(event.getSource() == registerBtn){
			//Notification.show("Not implemented yet", Type.HUMANIZED_MESSAGE);
			getUI().getNavigator().addView("/signup", SignUpScreen.class);
			getUI().getNavigator().navigateTo("/signup");
		}
	}
}
