package kg.vaadin.example.shiro;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.themes.Reindeer;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ErrorView extends VerticalLayout implements View{

	public ErrorView(){
		Label errorLabel = new Label("Oops. The view you tried to doesn't exist.");
		errorLabel.setStyleName(Reindeer.LABEL_H1);
		errorLabel.setSizeUndefined();
		addComponent(errorLabel);
		setComponentAlignment(errorLabel, Alignment.MIDDLE_CENTER);
	}
	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
	}
}
