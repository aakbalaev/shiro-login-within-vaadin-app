package kg.vaadin.example.shiro;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.authc.credential.PasswordService;

import kg.vaadin.example.domain.Person;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class SignUpScreen extends VerticalLayout implements View{
	
	public static final String PERSISTENCE_UNIT = "persistenceUnit";
	private static PasswordService passwordService;

	private Panel registerPanel = new Panel("Registration Form");
	private FormLayout formLayout = new FormLayout();
	private HorizontalLayout horizontalLayout = new HorizontalLayout();
	private TextField username = new TextField("Username");
	private TextField email = new TextField("Email");
	private PasswordField password = new PasswordField("Password");
	private PasswordField rePassword = new PasswordField("Reenter Password");
	private Button registerBtn = new Button("Register");
	private Button cancelBtn = new Button("Cancel");
	private Label statusBar = new Label("");
	
	public SignUpScreen(){
		setSizeFull();
		registerPanel.setSizeUndefined();
		username.focus();
		formLayout.addComponent(username);
		formLayout.addComponent(email);
		formLayout.addComponent(password);
		formLayout.addComponent(rePassword);
		horizontalLayout.addComponent(cancelBtn);
		horizontalLayout.addComponent(registerBtn);
		formLayout.addComponent(horizontalLayout);
		formLayout.addComponent(statusBar);
		formLayout.setSizeUndefined();
		formLayout.setMargin(true);
		registerPanel.setContent(formLayout);
		addComponent(registerPanel);
		setComponentAlignment(registerPanel, Alignment.MIDDLE_CENTER);
		
		cancelBtn.addClickListener(new ButtonListener());
		registerBtn.addClickListener(new ButtonListener());
	}
	
	class ButtonListener implements Button.ClickListener {

		@Override
		public void buttonClick(ClickEvent event) {
			if(event.getSource() == registerBtn){
				try {
					passwordService = new DefaultPasswordService();
					PasswordMatcher matcher = new PasswordMatcher();
					matcher.setPasswordService(passwordService);
					registerUser(username.getValue(), email.getValue(), password.getValue(), rePassword.getValue());
				} catch (Exception e) {
					Notification.show("Try to fix the fields entered, please", Type.ERROR_MESSAGE);
				}
			}
			else if (event.getSource() == cancelBtn){
				getUI().getNavigator().navigateTo("/");
			}
		}

		private void registerUser(String username, String email, String password, String rePassword){
			if (username.isEmpty() || email.isEmpty() || password.isEmpty() || rePassword.isEmpty() || !password.equals(rePassword)){
				Notification.show("Try to fix the errors in the field(s)", Type.ERROR_MESSAGE);
			}
			else{
				EntityManager em = Persistence	.createEntityManagerFactory(PERSISTENCE_UNIT).createEntityManager();
				em.getTransaction().begin();
				Person newUser = new Person(username, email, passwordService.encryptPassword(password));
				em.persist(newUser);
				em.getTransaction().commit();
				em.clear();
				em.close();
				Notification.show("Registration successful", Type.HUMANIZED_MESSAGE);
				getUI().getNavigator().navigateTo("/");
			}
		}
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}
}
