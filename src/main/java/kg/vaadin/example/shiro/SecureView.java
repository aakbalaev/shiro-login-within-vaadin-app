package kg.vaadin.example.shiro;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FileResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Audio;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

@SuppressWarnings("serial")
public class SecureView extends VerticalLayout implements View{
	private boolean initialized;
	
	private void build(){
		Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO, "Initializing secure view");
		Label secret = new Label("Super secret documentation of your project");
		secret.setStyleName(Reindeer.LABEL_H1);
		secret.setSizeUndefined();
		Button logoutBtn = new Button("Logout");
		logoutBtn.addClickListener(new ButtonListener());
		
		Label guestLabel = new Label("At the moment nothing for you");
		guestLabel.setSizeUndefined();
		guestLabel.setStyleName(Reindeer.LABEL_H1);
		
		Subject currentUser = SecurityUtils.getSubject();
    	Label username = new Label("Logged in as " +  currentUser.getPrincipal().toString());
    	username.setSizeUndefined();
    	username.setStyleName(Reindeer.LABEL_H2);
		
		BrowserFrame embedded = new BrowserFrame();
		embedded.setSource(new ThemeResource("secret.pdf"));
		embedded.setWidth("600px");
		embedded.setHeight("400px");
		
		// Find the application directory
		String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
		// Image as a file resource
		FileResource imageResource = new FileResource(new File(basepath +	"/WEB-INF/images/image.png"));
		// Show the image in the application
		Image image = new Image("Image from file", imageResource);
		
		addComponent(logoutBtn);
		setComponentAlignment(logoutBtn, Alignment.TOP_RIGHT);
		
		addComponent(username);
		setComponentAlignment(username, Alignment.TOP_CENTER);
		
		addComponent(secret);
		setComponentAlignment(secret, Alignment.TOP_CENTER);
		//secret.setVisible(false);
		
		addComponent(embedded);
		setComponentAlignment(embedded, Alignment.TOP_CENTER);
		//embedded.setVisible(false);
		
		addComponent(image);
		setComponentAlignment(image, Alignment.TOP_CENTER);
		//image.setVisible(false);
		
		addComponent(guestLabel);
		//guestLabel.setVisible(false);
		setComponentAlignment(guestLabel, Alignment.TOP_CENTER);
		
		if (!currentUser.hasRole("*"))
    	{
			secret.setVisible(false);
			embedded.setVisible(false);
    	}
		if(!currentUser.hasRole("manager")){
			image.setVisible(false);
		}
		if(!currentUser.hasRole("simple")){
			guestLabel.setVisible(false);
		}
	}
	@Override
	public void enter(ViewChangeEvent event) {
		if(!initialized){
			build();
			initialized = true;
		}
	}
	
	class ButtonListener implements Button.ClickListener{
		@Override
		public void buttonClick(ClickEvent event) {
			Subject currentUser = SecurityUtils.getSubject();
			if (currentUser.isAuthenticated())
			{
				currentUser.logout();
				logout();
			}
		}
		private void logout() {
	        // Close the VaadinServiceSession
	        getUI().getSession().close();

	        // Invalidate underlying session instead if login info is stored there
	        // VaadinService.getCurrentRequest().getWrappedSession().invalidate();

	        // Redirect to avoid keeping the removed UI open in the browser
	        getUI().getPage().setLocation("");
	    }
	}
}